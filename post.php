<?php include "includes/db.php" ?>
<?php 
include "includes/header.php"
 ?>

    <!-- Navigation -->
    <?php include "includes/nav.php" ?>

    <!-- Page Content -->
<div class="container">

<div class="row">

    <!-- Blog Entries Column -->
    <div class="col-md-8">
        <?php 
        if (isset($_GET['p_id'])) {
            $p_id = $_GET['p_id'];
        }

        $query = "SELECT * FROM posts WHERE post_id = {$p_id}";
        $select_post_query = mysqli_query($connection,$query);
        while ($row = mysqli_fetch_assoc($select_post_query)) {
            $post_id = $row['post_id'];
            $post_title = $row['post_title'];
            $post_author = $row['post_author'];
            $post_date = $row['post_date'];
            $post_image = $row['post_image'];
            $post_content = $row['post_content'];

            ?>

        <h1 class="page-header">
            Page Heading
            <small>Secondary Text</small>
        </h1>

        <!-- First Blog Post -->
        <h2>
            <a href=""><?php echo $post_title  ?></a>
        </h2>
        <p class="lead">
            by <a href="index.php"><?php echo $post_author ?></a>
        </p>
        <p><span class="glyphicon glyphicon-time"></span><?php echo $post_date  ?></p>
        <hr>
        <img class="img-responsive" src="images/<?php echo $post_image ?>" alt="">
        <hr>
        <p><?php echo $post_content ?> </p>
        <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

        <hr>
    <?php } ?>
     <!-- Blog Comments -->
        <?php 

            if (isset($_POST['create_comment'])) {
                $cmt_post_id = $_GET['p_id'];
                $cmt_author = $_POST['cmt_author'];
                $cmt_email = $_POST['cmt_email'];
                $cmt_content = $_POST['cmt_content'];
                $post_date = date('d-m-y');

                $query = "INSERT INTO comments (cmt_post_id, cmt_date, cmt_author, cmt_email, cmt_content, cmt_status) ";
                $query .="VALUES ({$cmt_post_id}, now(), '{$cmt_author}', '{$cmt_email}', '{$cmt_content}', 'Approved')";

                $cmt_insert_query = mysqli_query($connection, $query);
                if (!$cmt_insert_query) {
                    die ("QUERY FAILED " . mysqli_error($connection));
                }

                $query = "UPDATE posts SET post_comment_count = post_comment_count + 1 WHERE post_id = $p_id";
                $show_cmt_query = mysqli_query($connection, $query);
                if (!$show_cmt_query) {
                    die ("QUERY FAILED " . mysqli_error($connection));
                }
            }

         ?>



                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form action="" method="post" role="form">
                        <div class="form-group">
                            <label>Author</label>
                            <input class="form-control" type="text" name="cmt_author">
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input class="form-control" type="email" name="cmt_email">
                        </div>
                        <div class="form-group">
                            <label>Comment</label>
                            <textarea name="cmt_content" class="form-control" rows="3"></textarea>
                        </div>
                        <button type="submit" name="create_comment" class="btn btn-primary">Submit</button>
                    </form>
                </div>

                <hr>

                <!-- Posted Comments -->

                <?php 

                $query = "SELECT * FROM comments WHERE cmt_post_id = {$p_id} ";
                $query .= "AND cmt_status = 'approved' ";
                $query .= "ORDER BY cmt_id DESC ";
                $select_comment_query = mysqli_query($connection, $query);
                if (!$select_comment_query) {
                    die("QUERY FAILED " . mysqli_error($connection));
                }

                while ($row = mysqli_fetch_assoc($select_comment_query)) {
                    $cmt_id = $row['cmt_id'];
                    $cmt_date = $row['cmt_date'];
                    $cmt_author = $row['cmt_author'];
                    $cmt_content = $row['cmt_content'];


                 ?>


                <!-- Comment -->
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading"><?php echo $cmt_author; ?>
                            <small><?php echo $cmt_date; ?></small>
                        </h4>
                        <?php echo $cmt_content; ?>
                    </div>
                </div>
                <?php } ?>


                <!-- Comment -->
                
</div>

    <!-- Blog Sidebar Widgets Column -->
    <?php include "includes/sidebar.php" ?>

</div>
<!-- /.row -->

<hr>

<?php 
    include "includes/footer.php"
?>
