<div class="well">
    <h4>Login</h4>
    <form action="includes/login.php" method="post">
    	<div class="form-group">
    		<input class="form-control" type="text" placeholder="Enter Username" name="username">
    	</div>
    	<div class="input-group">
    		<input class="form-control" type="password" placeholder="Enter P assword" name="user_password">
    		<span class="input-group-btn">
    			<button class="btn btn-primary" name="login">Login</button>
    		</span>
    	</div>
    </form>
</div>