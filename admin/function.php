<?php 
	function insert_categories(){ //Inserts categories on the admin categories
		global $connection;
		if (isset($_POST['submit'])) {
            $cat_title = $_POST['cat_title'];

            if ($cat_title == "" || empty($cat_title)) {
                echo "<h1>The field is empty</h1>";
            }else{
                $query = "INSERT INTO categories(cat_title) VALUES('{$cat_title}') ";
                $add_categories_query = mysqli_query($connection, $query);
                if (!$add_categories_query) {
                    die ("Query Failed" . mysqli_error($connection));
                }
            }
        }
	}
    // shows categories on admin categories
    function show_all_categories(){ //shows all the categories in the admin categories
       global $connection;
        $query = "SELECT * FROM categories";
        $select_cat_query = mysqli_query($connection, $query);
        while ($row = mysqli_fetch_assoc($select_cat_query)) {
             $cat_id = $row['cat_id'];
             $cat_title = $row['cat_title'];
        echo "<tr>";
        echo"<td>{$cat_id}</td>";
        echo"<td>{$cat_title}</td>";
        echo "<td><a href='categories.php?delete={$cat_id}'>Delete</a></td>";
        echo "<td><a href='categories.php?edit={$cat_id}'>Update</a></td>";
        echo "</tr>";
         }  
        if (isset($_GET['delete'])) {
            $get_cat_id = $_GET['delete'];
            $query = "DELETE FROM categories WHERE cat_id = {$get_cat_id}";
            $delete_query = mysqli_query($connection, $query);
            if (!$delete_query) {
                die("Query Failed " . mysqli_error($connection));
            }
            header("Location: categories.php");
        }
    }
    //

 ?>