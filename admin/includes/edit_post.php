<?php 
  if (isset($_GET['p_id'])) {
    $the_post_id = $_GET['p_id'];

    $query = "SELECT * FROM posts WHERE post_id = $the_post_id ";
    $select_edit_post = mysqli_query($connection, $query);
    while ($row = mysqli_fetch_assoc($select_edit_post)) {
       $post_author = $row['post_author'];
       $post_title = $row['post_title'];
       $post_cat_id = $row['post_cat_id'];
       $post_status = $row['post_status'];
       $post_image = $row['post_image'];
       $post_tags = $row['post_tags'];
       $post_content = $row['post_content'];
       $post_date = $row['post_date'];
       $post_comment_count = $row['post_comment_count'];

    }
  }
  if (isset($_POST['edit_post'])) {
        $post_title = $_POST['post_title'];
        $post_status = $_POST['post_status'];
        $post_cat_id = $_POST['post_category'];
        $post_author = $_POST['post_author'];
        $post_image = $_FILES['post_image']['name'];
        $post_image_tmp = $_FILES['post_image']['tmp_name'];
        $post_tags = $_POST['post_tags'];
        $post_content = $_POST['post_content'];
        move_uploaded_file($post_image_tmp, "../images/$post_image" );


        if (empty($post_image)) {
          $query = "SELECT * FROM posts WHERE post_id = $the_post_id";
          $img_update = mysqli_query($connection, $query);
          while ($row = mysqli_fetch_assoc($img_update)) {
            $post_image = $row['post_image'];
          }
        }

        $query = "UPDATE posts SET ";
        $query .="post_title = '{$post_title}', ";
        $query .="post_cat_id = '{$post_cat_id}', ";
        $query .="post_date = now(), ";
        $query .="post_author = '{$post_author}', ";
        $query .="post_image = '{$post_image}', ";
        $query .="post_tags = '{$post_tags}', ";
        $query .="post_content = '{$post_content}', ";
        $query .="post_status = '{$post_status}' ";
        $query .= "WHERE post_id = {$the_post_id} ";

        $update_post = mysqli_query($connection ,$query);
        if (!$update_post) {
          die ("Query Failed" . mysqli_error($connection));
        }
  }

 ?>



<form action="" method="post" enctype="multipart/form-data">
   <div class="form-group">
        <label for="post_title">Post Title</label>
        <input value="<?php echo $post_title; ?>" class="form-control" type="text" name="post_title">
   </div>
   <div class="form-group">
        <label for="post_category">Post Category</label>
        <select name="post_category" class="form-control">
          <?php 
            $query = "SELECT * FROM categories"; 
            $select_edit_cat_query = mysqli_query($connection, $query);

            while ($row = mysqli_fetch_assoc($select_edit_cat_query)) {
             $cat_id = $row['cat_id'];
             $cat_title = $row['cat_title'];
             echo "<option value='$cat_id'>{$cat_title}</option>";
            }
           ?>
        </select>
   </div>
   <div class="form-group">
        <label for="post_author">Post Author</label>
        <input value="<?php echo $post_author; ?>" class="form-control" type="text" name="post_author">
   </div>
   <div class="form-group">
        <label for="post_image">Post Image</label><br>
        <img name="post_image" width="100" height="60" src="../images/<?php echo $post_image; ?>">
        <input type="file" name="post_image">
   </div>
   <div class="form-group">
        <label for="">Post Tags</label>
        <input value="<?php echo $post_tags; ?>" class="form-control" type="text" name="post_tags">
   </div>
   <div class="form-group">
        <label for="">Post Status</label>
        <input value="<?php echo $post_status; ?>" class="form-control" type="text" name="post_status">
   </div>
   <div class="form-group">
        <label for="post_content">Post Content</label>
        <textarea value="" class="form-control" name="post_content" cols="30" rows="10"><?php echo $post_content; ?></textarea>
   </div>
   <div class="form-group">
        <button class="btn btn-primary" type="submit" name="edit_post" value="">Update Post</button>
   </div>
</form> 