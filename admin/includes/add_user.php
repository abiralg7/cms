<?php 
    if (isset($_POST['create_user'])) {
        $user_firstname = $_POST['user_firstname'];
        $user_lastname = $_POST['user_lastname'];
        $username = $_POST['username'];
        $user_password = $_POST['user_password'];
        $user_image = $_FILES['user_image']['name'];
        $user_image_tmp = $_FILES['user_image']['tmp_name'];

        $user_email = $_POST['user_email'];
        $user_role = $_POST['role'];

        move_uploaded_file($user_image_tmp, "../images/$user_image");

        $query = "INSERT INTO users(user_firstname, user_lastname, username, user_password, user_image, user_email, role) VALUES ('{$user_firstname}', '{$user_lastname}', '{$username}', '{$user_password}', '{$user_image}', '{$user_email}', '{$user_role}')";
        $add_post_query = mysqli_query($connection, $query);
        if (!$add_post_query) {
            die ("Query Failed" . mysqli_error($connection));
        }
    }
 ?>


<form action="" method="post" enctype="multipart/form-data">
   <div class="form-group">
        <label for="first_name">First Name</label>
        <input class="form-control" type="text" name="user_firstname">
   </div>
   <div class="form-group">
        <label for="last_name">Last Name</label>
         <input class="form-control" type="text" name="user_lastname">
   </div>
   <div class="form-group">
        <label for="role">Role</label>
        <select class="form-control" name="role">
          <option value='suscriber'>Select Options</option>
          <option value='admin'>Admin</option>
          <option value='suscriber'>Suscriber</option>
        </select>
   </div>
   <div class="form-group">
        <label for="username">Username</label>
        <input class="form-control" type="text" name="username">
   </div>
   <div class="form-group">
        <label for="user_password">Password</label>
        <input class="form-control" type="password" name="user_password">
   </div>
   <div class="form-group">
        <label for="">Email</label>
        <input class="form-control" type="email" name="user_email">
   </div>
   <div class="form-group">
        <label for="user_image">User Image</label>
        <input type="file" name="user_image">
   </div>
   
   
   <div class="form-group">
        <button class="btn btn-primary" type="submit" name="create_user" value="">Add User</button>
   </div>
</form> 