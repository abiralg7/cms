<?php 
    if (isset($_POST['create_post'])) {
        $post_title = $_POST['post_title'];
        $post_category = $_POST['post_category'];
        $post_author = $_POST['post_author'];

        $post_image = $_FILES['post_image']['name'];
        $post_image_tmp = $_FILES['post_image']['tmp_name'];

        $post_tags = $_POST['post_tags'];
        $post_content = $_POST['post_content'];
        $post_date = date('d-m-y');

        move_uploaded_file($post_image_tmp, "../images/$post_image" );

        $query = "INSERT INTO posts(post_title, post_cat_id, post_author, post_image, post_tags, post_content, post_date) VALUES('{$post_title}', {$post_category}, '{$post_author}', '{$post_image}', '{$post_tags}', '{$post_content}', now()) ";
        $add_post_query = mysqli_query($connection, $query);
        if (!$add_post_query) {
            die ("Query Failed" . mysqli_error($connection));
        }
    }
 ?>


<form action="" method="post" enctype="multipart/form-data">
   <div class="form-group">
        <label for="post_title">Post Title</label>
        <input class="form-control" type="text" name="post_title">
   </div>
   <div class="form-group">
        <label for="post_category">Post Category</label>
         <select name="post_category" class="form-control">
          <?php 
            $query = "SELECT * FROM categories"; 
            $select_edit_cat_query = mysqli_query($connection, $query);

            while ($row = mysqli_fetch_assoc($select_edit_cat_query)) {
             $cat_id = $row['cat_id'];
             $cat_title = $row['cat_title'];
             echo "<option value='$cat_id'>{$cat_title}</option>";
            }
           ?>
        </select>
   </div>
   <div class="form-group">
        <label for="post_author">Post Author</label>
        <input class="form-control" type="text" name="post_author">
   </div>
   <div class="form-group">
        <label for="post_image">Post Image</label>
        <input type="file" name="post_image">
   </div>
   <div class="form-group">
        <label for="">Post Tags</label>
        <input class="form-control" type="text" name="post_tags">
   </div>
   <div class="form-group">
        <label for="">Post Status</label>
        <input class="form-control" type="text" name="post_status">
   </div>
   <div class="form-group">
        <label for="post_content">Post Content</label>
        <textarea class="form-control" name="post_content" cols="30" rows="10"></textarea>
   </div>
   <div class="form-group">
        <button class="btn btn-primary" type="submit" name="create_post" value="">Publish Post</button>
   </div>
</form> 