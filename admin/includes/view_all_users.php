<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Id</th>
            <th>First Name</th>
            <!-- <th>Last Name</th> -->
            <th>Username</th>
            <th>Password</th>
            <th>Email</th>
            <th>Profile Picture</th>
            <th>Role</th>
            <th>Admin</th>
            <th>Suscriber</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $query = "SELECT * FROM users";
            $select_users_query = mysqli_query($connection, $query);
            while ($row = mysqli_fetch_assoc($select_users_query)) {
             $user_id = $row['user_id'];
             $username = $row['username'];
             $user_password = $row['user_password'];
             $user_firstname = $row['user_firstname'];
             $user_lastname = $row['user_lastname'];
             $user_email = $row['user_email'];
             $user_image = $row['user_image'];
             $user_role = $row['role'];
                echo "<tr>";
                echo "<td>{$user_id}</td>";
                echo "<td>{$user_firstname} {$user_lastname}</td>";
                echo "<td>{$username}</td>";
                echo "<td>{$user_password}</td>";
                echo "<td>{$user_email}</td>";
                echo "<td><img width='100' height='60' src='../images/{$user_image}'></td>";
                echo "<td>{$user_role}</td>";
                echo "<td><a href='users.php?change_to_admin={$user_id}'>admin</a></td>";
                echo "<td><a href='users.php?change_to_sub={$user_id}'>suscriber</a></td>";
                echo "<td><a href='users.php?source=edit_user&u_id={$user_id}'>edit</a></td>";
                echo "<td><a href='users.php?delete=$user_id'>delete</a></td>";
                echo "</tr>";

            if (isset($_GET['change_to_admin'])) {
                 $change_to_admin = $_GET['change_to_admin'];
                 $query = "UPDATE users SET role = 'admin' WHERE user_id = $change_to_admin";
                 $change_admin_query = mysqli_query($connection, $query);
                 if (!$change_admin_query) {
                     die ("Query Failed" . mysqli_error($connection));
                 }
                 header("Location:users.php");
             }

             if (isset($_GET['change_to_sub'])) {
                 $change_to_sub = $_GET['change_to_sub'];
                 $query = "UPDATE users SET role = 'suscribe' WHERE user_id = $change_to_sub";
                 $change_sub_query = mysqli_query($connection, $query);
                 if (!$change_sub_query) {
                     die ("Query Failed" . mysqli_error($connection));
                 }
                 header("Location:users.php");
             }


             if (isset($_GET['delete'])) {
                 $get_user_id = $_GET['delete'];
                 $query = "DELETE FROM users WHERE user_id = {$get_user_id}";
                 $delete_query = mysqli_query($connection, $query);
                 if (!$delete_query) {
                     die ("Query Failed" . mysqli_error($connection));
                 }
                 header("Location:users.php");
             }
        }
         ?>
    </tbody>
</table>