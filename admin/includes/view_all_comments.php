<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Id</th>
            <th>Author</th>
            <th>Email</th>
            <th>In Response To</th>
            <th>Comment</th>
            <th>Status</th>
            <th>Date</th>
            <th>Approve</th>
            <th>Unapprove</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $query = "SELECT * FROM comments";
            $select_comments_query = mysqli_query($connection, $query);
            while ($row = mysqli_fetch_assoc($select_comments_query)) {
             $cmt_id = $row['cmt_id'];
             $cmt_author = $row['cmt_author'];
             $cmt_email = $row['cmt_email'];
             $cmt_post_id = $row['cmt_post_id'];
             $cmt_status = $row['cmt_status'];
             $cmt_content = $row['cmt_content'];
             $cmt_date = $row['cmt_date'];
                echo "<tr>";
                echo "<td>{$cmt_id}</td>";
                echo "<td>{$cmt_author}</td>";
                echo "<td>{$cmt_email}</td>";


            $query = "SELECT * FROM posts WHERE post_id = {$cmt_post_id}";
            $select_edit_post_query = mysqli_query($connection, $query);

            while ($row = mysqli_fetch_assoc($select_edit_post_query)) {
             $post_id = $row['post_id'];
             $post_title = $row['post_title'];
                echo "<td><a href='../post.php?p_id=$post_id'>{$post_title}</a></td>";
            }
                

                echo "<td>{$cmt_content}</td>";
                echo "<td>{$cmt_status}</td>";
                echo "<td>{$cmt_date}</td>";
                echo "<td><a href='comments.php?approve={$cmt_id}'>approve</a></td>";
                echo "<td><a href='comments.php?unapprove={$cmt_id}'>unapprove</a></td>";
                // echo "<td><a href='comments.php?source=edit_comment&cmt_id={$cmt_id}'>edit</a></td>";
                echo "<td><a href='comments.php?delete={$cmt_id}'>delete</a></td>";
                echo "</tr>";

            if (isset($_GET['approve'])) {
                 $approve_comment_id = $_GET['approve'];
                 $query = "UPDATE comments SET  cmt_status = 'approved' WHERE cmt_id = $approve_comment_id ";
                 $approve_cmt_query = mysqli_query($connection, $query);
                 if (!$approve_cmt_query) {
                     die ("Query Failed" . mysqli_error($connection));
                 }
                 header("Location:comments.php");
             }


              if (isset($_GET['unapprove'])) {
                 $unapprove_comment_id = $_GET['unapprove'];
                 $query = "UPDATE comments SET  cmt_status = 'unapproved' WHERE cmt_id = $unapprove_comment_id";
                 $unapprove_cmt_query = mysqli_query($connection, $query);
                 if (!$unapprove_cmt_query) {
                     die ("Query Failed" . mysqli_error($connection));
                 }
                 header("Location:comments.php");
             }


             if (isset($_GET['delete'])) {
                 $get_comment_id = $_GET['delete'];
                 $query = "DELETE FROM comments WHERE cmt_id = {$get_comment_id}";
                 $delete_query = mysqli_query($connection, $query);
                 if (!$delete_query) {
                     die ("Query Failed" . mysqli_error($connection));
                 }
                 header("Location:comments.php");
             }
        }
         ?>
    </tbody>
</table>