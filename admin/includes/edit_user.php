<?php 
  if (isset($_GET['u_id'])) {
    $the_user_id = $_GET['u_id'];

    $query = "SELECT * FROM users WHERE user_id = $the_user_id";
    $select_user = mysqli_query($connection, $query);
    if (!$select_user) {
      die("QUERY FAILED " . mysqli_error($connection));
    }
    while ($row = mysqli_fetch_assoc($select_user)) {
        $user_firstname = $row['user_firstname'];
        $user_lastname = $row['user_lastname'];
        $username = $row['username'];
        $user_password = $row['user_password'];
        $user_image = $row['user_image'];
        $user_email = $row['user_email'];
        $user_role = $row['role'];
    }
  }

 ?>


<?php 
    if (isset($_POST['edit_user'])) {
        $user_firstname = $_POST['user_firstname'];
        $user_lastname = $_POST['user_lastname'];
        $username = $_POST['username'];
        $user_password = $_POST['user_password'];

        $user_image = $_FILES['user_image']['name'];
        $user_image_tmp = $_FILES['user_image']['tmp_name'];

        $user_email = $_POST['user_email'];
        $user_role = $_POST['role'];
        move_uploaded_file($user_image_tmp, "../images/$user_image" );


        if (empty($user_image)) {
          $query = "SELECT * FROM users WHERE user_id = $the_user_id";
          $img_update = mysqli_query($connection, $query);
          while ($row = mysqli_fetch_assoc($img_update)) {
            $user_image = $row['user_image'];
          }
        }

        $query = "UPDATE users SET ";
        $query .="user_firstname = '{$user_firstname}', ";
        $query .="user_lastname = '{$user_lastname}', ";
        $query .="username = '{$username}', ";
        $query .="user_password = '{$user_password}', ";
        $query .="user_email = '{$user_email}', ";
        $query .="user_image = '{$user_image}', ";
        $query .="role = '{$user_role}' ";
        $query .= "WHERE user_id = {$the_user_id} ";

        $update_post = mysqli_query($connection ,$query);
        if (!$update_post) {
          die ("Query Failed" . mysqli_error($connection));
        }
  }
 ?>


<form action="" method="post" enctype="multipart/form-data">
   <div class="form-group">
        <label for="first_name">First Name</label>
        <input value="<?php echo $user_firstname; ?>" class="form-control" type="text" name="user_firstname">
   </div>
   <div class="form-group">
        <label for="last_name">Last Name</label>
         <input value="<?php echo $user_lastname; ?>" class="form-control" type="text" name="user_lastname">
   </div>
   <div class="form-group">
        <label for="role">Role</label>
        <select class="form-control" name="role">
          <option value='suscriber'><?php echo $user_role; ?></option>
          <?php 
            if ($user_role == 'admin') {
            echo"<option value='suscriber'>suscriber</option>";
            }
            if ($user_role == 'suscriber') {
            echo "<option value='admin'>admin</option>";
            }

           ?>
        </select>
   </div>
   <div class="form-group">
        <label for="username">Username</label>
        <input value="<?php echo $username; ?>" class="form-control" type="text" name="username">
   </div>
   <div class="form-group">
        <label for="user_password">Password</label>
        <input value="<?php echo $user_password; ?>" class="form-control" type="password" name="user_password">
   </div>
   <div class="form-group">
        <label for="">Email</label>
        <input value="<?php echo $user_email; ?>" class="form-control" type="email" name="user_email">
   </div>
   <div class="form-group">
        <label for="user_image">User Image</label>
        <img name="user_image" width="100" height="60" src="../images/<?php echo $user_image; ?>">
        <input value="<?php echo $user_image; ?>" type="file" name="user_image">
   </div>
   
   
   <div class="form-group">
        <button class="btn btn-primary" type="submit" name="edit_user" value="">Update User</button>
   </div>
</form> 